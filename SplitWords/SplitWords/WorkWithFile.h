#include <vector>
#pragma once
class WorkWithFile
{
public:
	WorkWithFile();
	~WorkWithFile();

	std::vector<std::string> SortingWords(const std::string &string);
private:
	int CountWordsInString(const std::string &string);
	std::vector<std::string> SplitString(const std::string &string);
};