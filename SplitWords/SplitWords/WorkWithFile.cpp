#include "WorkWithFile.h"
#include <vector>

WorkWithFile::WorkWithFile()
{
}

WorkWithFile::~WorkWithFile()
{
}

std::vector<std::string> WorkWithFile::SortingWords(const std::string &string)
{
	int totalCountWords = WorkWithFile::CountWordsInString(string);
	std::vector<std::string> destionationString = SplitString(string);

	for (size_t i = 0; i < totalCountWords; i++)
	{
		for (size_t j = 0; j < totalCountWords; j++)
		{
			std::string word1 = destionationString.at(j);
			std::string word2 = destionationString.at(j + 1);

			int word1Len = word1.size();
			int word2Len = word2.size();

			if (word1Len < word2Len)
			{
				std::string tmp = word1;
				word1 = word2;
				word2 = tmp;
			}

			destionationString.at(j) = word1;
			destionationString.at(j + 1) = word2;
		}
	}

	return destionationString;
}

std::vector<std::string> WorkWithFile::SplitString(const std::string &string)
{
	std::vector<std::string> splitedString;

	std::string splitedWord = "";
	std::string separator = " ";

	char* ConvertToChar = const_cast<char*>(string.c_str());
	splitedWord = strtok(ConvertToChar, separator.c_str());
	splitedString.push_back(splitedWord);

	int totalCountWords = WorkWithFile::CountWordsInString(string);
	for (size_t i = 0; i < totalCountWords; i++)
	{
		splitedWord = strtok(0, separator.c_str());
		splitedString.push_back(splitedWord);
	}

	return splitedString;
}

int WorkWithFile::CountWordsInString(const std::string &string)
{
	int count = 0;

	for (size_t i = 0; i < string.size(); i++)
	{
		if (string[i] == ' '
			|| string[i] == '\n'
			|| string[i] == '\t'
			|| string[i] == '\0')
			count++;
	}

	return count;
}