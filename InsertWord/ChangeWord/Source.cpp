#include <iostream>
#include "WorkWithString.h"

int main()
{
	setlocale(LC_ALL, "Russian");

	const int size = 1024;

	std::string string;
	char word[size];
	int index;

	std::cout << "������� ������:";
	std::cin >> string;

	std::cout << "������� �����:";
	std::cin >> word;

	std::cout << "������� ������";
	std::cin >> index;

	WorkWithString w;
	std::string newString = w.CutString(string);
	newString = w.InsertWordInString(newString, word, index);

	std::cout << newString.c_str() << std::endl;
}
/*�������6:
�������� ���������, ������� ��������� ������ � ����������, ������ �������� �� 1024 ��������. 
����� ������ ������ �����, ����� ������ ������ ������. 
����� ��������� ��������� ����� � ������ � ���������� �������. 
����������� ����� �� ������ �������� ������������*/