#include <string>
#pragma once
class WorkWithString
{
public:
	WorkWithString();
	~WorkWithString();

	std::string InsertWordInString(std::string &string, const std::string &word, const int &index);
	std::string CutString(std::string &string);
};