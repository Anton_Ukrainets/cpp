#include "StringHelper.h"

StringHelper::StringHelper(const std::string &path)//��������� � ������������ ���� � �����
{
	this->path = path;

	long fileSize = this->FileSize();
	std::string string = this->ReadToFile(fileSize);//read

	std::vector<std::string> words = this->SplitString(string);//split
	std::vector<std::string> reversedString = this->ReverseWords(words);//reverse
	std::string newString = this->ConcatenationInString(reversedString);//concat

	this->WriteToFile("1.txt", newString);//write
}

long StringHelper::FileSize()
{
	std::ifstream is(this->path, std::ios::binary);
	is.seekg(0, std::ios::end);
	long length = is.tellg();

	return length;
}

std::string StringHelper::ReadToFile(long fileSize)//������ � �����
{
	std::ifstream file(this->path);

	if (!file)
	{
		printf("File wasn't found\n");
		exit(1);
	}

	char* buffer = new char[fileSize];
	strcpy(buffer, "");//���� ���� ���� ������� �� ������ ���� ������ ��� �����-���� ������, ��� �����

	std::string line;
	if (file.is_open())
	{
		while (getline(file, line))
		{
			strcat(buffer, " ");
			strcat(buffer, line.c_str());
		}
	}

	file.close();

	return buffer;
}

std::vector<std::string> StringHelper::SplitString(const std::string &string)//����� ������
{
	const char separator[] = " \n";
	char* word = strtok((char*)string.c_str(), separator);

	std::vector<std::string> words;
	while (word)
	{
		words.push_back(word);
		word = strtok(0, separator);
	}

	return words;
}

std::vector<std::string> StringHelper::ReverseWords(const std::vector<std::string> &words)//������ ������ ����
{
	std::vector<std::string> reversedString;

	size_t countWords = words.size();
	for (int i = countWords - 1; i >= 0; i--)
		reversedString.push_back(words[i]);

	return reversedString;
}

std::string StringHelper::ConcatenationInString(const std::vector<std::string> &reversedString)//������������ ����
{
	std::string string;

	int vectorSize = reversedString.size();
	for (size_t i = 0; i < vectorSize; i++)
	{
		string += reversedString[i]; 
		
		if (i != vectorSize - 1)
			string += reversedString[i] + " ";
	}

	return string;
}

void StringHelper::WriteToFile(const std::string &filePath, const std::string &text)//������ � ����
{
	std::ofstream file(filePath, std::ios_base::out | std::ios_base::trunc);

	file << text.c_str();
	file.close();

	printf("Done\n");
}