#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#pragma once

class StringHelper
{
private:
	std::string path;//���� � �����

public:
	StringHelper(const std::string &path);//�����������

private:
	long FileSize();//���������� ������� �����

	std::string ReadToFile(long fileSize);//������ ������ � �����

	std::vector<std::string> SplitString(const std::string &string);//����� ������ �� �����
	std::vector<std::string> ReverseWords(const std::vector<std::string> &words);//�������������� �����
	std::string ConcatenationInString(const std::vector<std::string> &reversedString);//���������� ��� ������������ ����� � ������

	void WriteToFile(const std::string &filePath, const std::string &string);//���������� � ����
};