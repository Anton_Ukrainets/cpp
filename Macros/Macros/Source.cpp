#include <iostream>

#define VARIANT 1
#if VARIANT == 1
#include "Macro.h"
#elif VARIANT == 2
#include "FuncInline.h"
#else
#include "Func.h"
#endif VARIANT == 1

int main()
{
#if VARIANT == 1
	Macro m;
	m.ShowMIN(a, b);
	m.ShowMAX(a, b);
	m.ShowEV_NUM(a);
#elif VARIANT == 2
	FuncInline funcLine;
	funcLine.ShowMIN(a, b);
	funcLine.ShowMAX(a, b);
	funcLine.ShowEV_NUM(a);
#else
	Func func;
	func.ShowMIN(a, b);
	func.ShowMAX(a, b);
	func.ShowEV_NUM(a);
#endif

	setlocale(LC_ALL, "Russian");
	int a = 0, b = 0;

	std::cout << "������� ������ �����" << std::endl;
	std::cin >> a;

	std::cout << "������� ������ �����" << std::endl;
	std::cin >> b;

	int key = 0;
	std::cout << "1 - ������� ����������� �� ���� ��������� �����." << std::endl;
	std::cout << "2 - ������� ������������ �� ���� ��������� �����." << std::endl;
	std::cout << "3 - ���������� ������ ��� �� ������ �����." << std::endl;
	std::cin >> key;

	switch (key)
	{
	case 1:
	{  

		break;
	}
	case 2:
	{
#undef VARIANT
#define VARIANT 2
		break;
	}
	case 3:
	{
#undef VARIANT
#define VARIANT 3
	}
	default:
		break;
	}
}
/*� maine ���������� ���������� ������� ����� ����:
1 - ������� ����������� �� ���� ��������� �����. 
��� ������ ����� ���� ���������� ��������� ������������ ������ ������ �����, ����� ������ � ������� �� ������� ����������.
2 - ������� ������������ �� ���� ��������� �����. 
��� ������ ����� ���� ���������� ��������� ������������ ������ ������ �����, ����� ������ � ������� �� ������� ����������.
3 - ���������� ������ ��� �� ������ �����. 
��� ������ ����� ���� ���������� ��������� ������������ ������ ����� � ������� �� ������� ������ ��� ��� ���.
������������� ��������� #define VARIANT, ������� ���� ��������� 1, ����� ������������ ������� �� Macro.h, 2 - Func.h � 3 - FuncInline.h. 
���������� �� ���� �� ���� �������� ���������� ������������ ���� �� ���� ������������ �����, 
��� ���� ������ ������ �� ������ ��������� � main.
����������� ����������� � �������� ��� ����������. */