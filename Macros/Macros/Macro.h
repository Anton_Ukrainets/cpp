#include <iostream>
#pragma once
class Macro
{
public:
	Macro();
	~Macro();

#define A a
#define B b

#if A > B
	#define MIN B
#else
	#define MIN A
#endif A > B

	void ShowMIN(int a, int b)
	{
		std::cout << MIN << std::endl;
	}

#if A < B
	#define MAX B
#else
	#define MAX A
#endif // A < B

	void ShowMAX(int a, int b)
	{
		std::cout << MAX << std::endl;
	}

#if A % 2 == 0
	#define EV_NUM true
#else
	#define EV_NUM false
#endif // A % 2 == 0

	void ShowEV_NUM(int a)
	{
		if (EV_NUM)
			std::cout << "even" << std::endl;
		else
			std::cout << "odd" << std::endl;
	}
};
/*�������� � ������ Macro.h, 
� ������� ���������� ����������� ��������� �������:
1) ������������ ����������� �������� �� ���� MIN
2) ������������ ������������ �������� �� ���� MAX
3) �������� ����� �� �������� EV_NUM*/