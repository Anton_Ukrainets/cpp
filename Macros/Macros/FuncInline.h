#include <iostream>
using namespace std;
#pragma once
class FuncInline
{
public:
	FuncInline();
	~FuncInline();

#define A a
#define B b

#if A > B
	void ShowMIN(int a, int b)
	{
		cout << B << endl;
	}
#else
	void ShowMIN(int a, int b)
	{
		cout << A << endl;
	}
#endif A > B

#define A a
#define B b

#if A < B
	void ShowMAX(int a, int b)
	{
		cout << B << endl;
	}
#else
	void ShowMAX(int a, int b)
	{
		cout << A << endl;
	}
#endif A < B

#define A a
#define B b

#if A % 2 == 0
	void ShowEV_NUM(int a)
	{
		cout << "even" << endl;
	}
#else
	void ShowEV_NUM(int a)
	{
		cout << "odd" << endl;
	}
#endif A % 2 == 0
};
/*�������� � ������ ���� FuncInline.h, 
� ������� ��������� ����������� ���������� �������:
1) ���������� ����������� �������� �� ���� Min
2) ���������� ������������ �������� �� ���� Max
3) �������� ����� �� �������� EvNum*/