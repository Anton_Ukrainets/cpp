#include "Func.h"
#include <iostream>

Func::Func()
{
}

Func::~Func()
{
}

#define MIN(a, b) \
	if (a > b) std::cout << b << std::endl;\
	else std::cout << a << std::endl;

void Func::ShowMIN(int a, int b)
{
	MIN(a, b);
}

#define MAX(a, b) \
	if (a < b) std::cout << b << std::endl;\
	else std::cout << a << std::endl;

void Func::ShowMAX(int a, int b)
{
	MAX(a, b);
}

#define EV_NUM(a) \
	if (a % 2 == 0) std::cout << "even" << std::endl;\
	else std::cout << "odd" << std::endl;

void Func::ShowEV_NUM(int a)
{
	EV_NUM(a);
}

/*�������� Func.cpp, 
� ������� ���������� ����������� ������� ����������� � Func.h*/