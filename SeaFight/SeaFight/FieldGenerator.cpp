#include "FieldGenerator.h"
#include <random>
#include <time.h>

FieldGenerator::FieldGenerator()
{
	srand(time(NULL));
}


FieldGenerator::~FieldGenerator()
{
}

PlayerField FieldGenerator::GenerateField()
{
	PlayerField field;
	//���������� �������

	//1-4� ��������,
	GenerateShip(field, 4);

	//2-3� ��������,
	for (size_t i = 0; i < 2; i++)
	{
		GenerateShip(field, 3);
	}

	//3-2� ��������,
	for (size_t i = 0; i < 3; i++)
	{
		GenerateShip(field, 2);
	}

	//4-1� ��������
	for (size_t i = 0; i < 4; i++)
	{
		GenerateShip(field, 1);
	}

	return field;
}

void FieldGenerator::GenerateShip(PlayerField &field, int shipSize)
{
	bool generated = false;
	while (!generated)
	{
		//���������� ����

		int x = rand() % 10;
		int y = rand() % 10;

		//���������� ���������� �������
		int orientation = rand() % 2;

		//�������, ����� �� �� ���������
		//����� �� �������
		bool outOfRange = FieldGenerator::IsOutOfRange(x, y, orientation, shipSize);

		//����������� � ������� ���������
		bool hasIntersection = FieldGenerator::HasIntersection(field, x, y, orientation, shipSize);

		if (!outOfRange && !hasIntersection)
		{
			//���� �����, ����� ��������� ������� � ����
			FieldGenerator::SetShip(field, x, y, orientation, shipSize);

			generated = true;
		}
	}
}

void FieldGenerator::SetShip(PlayerField &field, int x, int y, int orientation, int shipSize)
{
	for (size_t i = 0; i < shipSize; i++)
	{
		if (orientation == 0)
			field.SetState(x + i, y, 1);
		else
			field.SetState(x, y + i, 1);
	}
}

bool FieldGenerator::IsOutOfRange(int x, int y, int orientation, int shipSize)
{
	bool outOfRange = false;

	if ((orientation == 0 && x + shipSize > 10)
		|| (orientation == 1 && y + shipSize > 10))
		outOfRange = true;

	return outOfRange;
}

bool FieldGenerator::HasIntersection(PlayerField & field, int x, int y, int orientation, int shipSize)
{
	if (orientation == 0)
	{
		for (int i = x - 1; i <= x + shipSize; i++)
		{
			for (int j = y - 1; j <= y + 1; j++)
			{
				if (field.GetState(i, j) == 1)
					return true;
			}
		}
	}
	else
	{
		for (int i = x - 1; i <= x + 1; i++)
		{
			for (int j = y - 1; j <= y + shipSize; j++)
			{
				if (field.GetState(i, j) == 1)
					return true;
			}
		}
	}

	return false;
}