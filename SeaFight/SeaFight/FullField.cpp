﻿#include "FullField.h"
#include "PlayerField.h"
#include <string>

FullField::FullField()
{
}

FullField::~FullField()
{
}

void FullField::Draw(PlayerField &player1Field, PlayerField &player1Attemps, PlayerField &player2Field, PlayerField &player2Attemps)
{
	string* field1 = DrawPlayerField(player1Field);
	string* field2 = DrawPlayerField(player1Attemps);

	for (size_t i = 0; i < 13; i++)
	{
		string fullFieldLine;

		fullFieldLine += Indents(2);
		fullFieldLine += field1[i];

		fullFieldLine += Indents(10);
		fullFieldLine += field2[i];

		cout << fullFieldLine << endl;
	}

	cout << endl;

	/*string* field3 = DrawPlayerField(player2Field);
	string* field4 = DrawPlayerField(player2Attemps);

	for (size_t i = 0; i < 13; i++)
	{
		string fullFieldLine;

		fullFieldLine += Indents(2);
		fullFieldLine += field3[i];

		fullFieldLine += Indents(10);
		fullFieldLine += field4[i];

		cout << fullFieldLine << endl;
	}*/
}

string FullField::Indents(int x)
{
	string line;

	for (int i = 0; i < x;i++)
		line += " ";

	return line;
}

string * FullField::DrawPlayerField(PlayerField &playerField)
{
	char letters[] = { "ABCDEFGHIJ" };

	string* field = new string[13];
	field[0] = "  1 2 3 4 5 6 7 8 9 10 ";

	field[1] = GetTopLine();

	for (size_t y = 0; y < 10; y++)
	{
		string line;

		line += letters[y];
		line += char(186);

		for (size_t x = 0; x < 10; x++)
		{
			int state = playerField.GetState(x, y);

			char symbol = FullField::StateToChar(state);

			line += symbol;
			line += " ";
		}

		line += char(186);

		field[y + 2] = line;
	}

	field[12] = GetBottomLine();

	return field;
}

char FullField::StateToChar(int state)
{
	if (state == 0) //пусто
		return ' ';
	else if (state == 1) // корабль
		return (char)219;
	else if (state == 2) // подбит
		return 'X';
	else if (state == 3) // мимо
		return (char)248;

	return ' ';
}

string FullField::GetTopLine()
{
	string line = " ";
	line += (char)201;

	for (int i = 0;i < 20;i++)
		line += (char)205;
	line += (char)187;

	return line;
}

string FullField::GetBottomLine()
{
	string line = " ";
	line += (char)200;

	for (int i = 0;i < 20;i++)
		line += (char)205;

	line += (char)188;

	return line;
}