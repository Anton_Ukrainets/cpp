#include "PlayerField.h"

PlayerField::PlayerField()
{
}


PlayerField::~PlayerField()
{
}

int PlayerField::GetState(int x, int y)
{
	return field[x][y];
}

void PlayerField::SetState(int x, int y, int state)
{
	field[x][y] = state;
}
