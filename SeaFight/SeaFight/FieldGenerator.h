#include "PlayerField.h"
#pragma once
class FieldGenerator
{
public:
	FieldGenerator();
	~FieldGenerator();

	PlayerField GenerateField();

private:
	void GenerateShip(PlayerField &field, int shipSize);
	void SetShip(PlayerField &field, int x, int y, int orientation, int shipSize);
	bool IsOutOfRange(int x, int y, int orientation, int shipSize);
	bool HasIntersection(PlayerField &field, int x, int y, int orientation, int shipSize);
};