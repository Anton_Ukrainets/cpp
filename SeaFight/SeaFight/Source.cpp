#include <iostream>
#include "PlayerField.h"
#include "FullField.h"
#include "FieldGenerator.h"
#include <string>

int GetFirstPlayer();

int main()
{
	FieldGenerator generator;

	// ���������
	PlayerField player1Field = generator.GenerateField();
	PlayerField player2Field = generator.GenerateField();

	PlayerField player1AttemptsField;
	PlayerField player2AttemptsField;

	FullField map;

	int currentPlayer = GetFirstPlayer();
	int winner = 0;

	bool finished = false;

	while (!finished)
	{
		map.Draw(player1Field, player1AttemptsField, player2Field, player2AttemptsField);

		int coordinateX = 0, coordinateY = 0;

		//������� ���
		if (currentPlayer == 1)
		{
			char coordinateYInput;

			std::cout << "Enter coordinate X:";
			std::cin >> coordinateX;

			std::cout << "Enter coordinate Y:";
			std::cin >> coordinateYInput;

			//��������� ����
			coordinateY = coordinateYInput - 64;

			coordinateX--;
			coordinateY--;
		}
		else
		{
			//����� ���������

			coordinateX = rand() % 10;
			coordinateY = rand() % 10;
		}

		// ����������
		bool hit = false;

		if (currentPlayer == 1)
		{
			int state = player2Field.GetState(coordinateX, coordinateY);
			if (player2Field.GetState(coordinateX, coordinateY) == 1)
			{
				hit = true;
				player1AttemptsField.SetState(coordinateX, coordinateY, 2);
			}
			else
			{
				player1AttemptsField.SetState(coordinateX, coordinateY, 3);
			}

		}
		else
		{
			if (player1Field.GetState(coordinateX, coordinateY) == 1)
			{
				hit = true;
				player2AttemptsField.SetState(coordinateX, coordinateY, 2);
			}
			else
			{
				player2AttemptsField.SetState(coordinateX, coordinateY, 3);
			}
		}

		//��������� ����������� �� ����
		finished = true;

		for (size_t i = 0; i < 10; i++)
		{
			for (size_t j = 0; j < 10; j++)
			{
				if (currentPlayer == 1)
				{
					if (player2Field.GetState(i, j) == 1
						&& player1AttemptsField.GetState(i, j) != 2)
						finished = false;
				}
				else
				{
					if (player1Field.GetState(i, j) == 1
						&& player2AttemptsField.GetState(i, j) != 2)
						finished = false;
				}
			}
		}

		if (finished)
			winner = currentPlayer;

		//�������� ����
		//���� �����, �� ��������� ���
		//���� ���, �� ����� 2 �����
		if (!hit)
		{
			if (currentPlayer == 1)
				currentPlayer = 2;
			else
				currentPlayer = 1;
		}

		if (!finished)
			system("cls");
		else
		{
			map.Draw(player1Field, player1AttemptsField, player2Field, player2AttemptsField);			
		}
	}

	cout << "Winner Player" << winner << endl;
	cout << "Game Over!";

	/*char x;
	cin >> x;*/
}

int GetFirstPlayer()
{
	return 1;
}

/*����������� ���� "������� ���", � ������� ����� ����� ������ � �����������.
��������� ���������� ���� �������� � ����:

1. ��������� �������� ���� ����������� (������� �������� ������� ���� �� ����������� �������� �� ����)
2. ����� "�������� ����" � �������
3. ���������� ��� (�������� ������, � ����������� �� ���������� �� ������� ���� ���������� - "����", "�����" , "����")

4. ��������� �������� ���� ������ (��������� ��������: �������, ��������, �� �����)
5. ����� "�������� ����" � �������
6. ���������� ��� (�������� ����������, � ����������� �� ���������� �� ������� ���� ������ - "����", "�����" , "����")

7. ������ ���� ������� � ����

�� ������ ������ ��������� ������������ ���� "������� ���", � ������� ��������� �������� ������� ���������, ��������� �������
�������� �� �������� � �.�. �� ������� ������������ ��� ���� - ���� ������, � "������� ����" ����������.*/