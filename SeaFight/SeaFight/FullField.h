#include "PlayerField.h"
#pragma once
class FullField
{
public:
	FullField();
	~FullField();

	void Draw(PlayerField &player1Field, PlayerField &player1Attemps, PlayerField &player2Field, PlayerField &player2Attemps);

private:
	string Indents(int x);
	string* DrawPlayerField(PlayerField &playerField);
	char StateToChar(int state);
	string GetTopLine();
	string GetBottomLine();
};