#include <vector>

#pragma once
class WorkWithFiles
{
public:
	WorkWithFiles();
	~WorkWithFiles();

	int CountWordsInFile(std::string pathToFile, std::string word);
	int CountWordsInDirectory(std::string pathToDirectory, std::string word);
private:
	std::vector<std::string> GetFilesFromDirectory(std::string pathToFile);
	int CountWordsInString(std::string str, std::string word);
	std::string GetFileContent(std::string filePath);
};