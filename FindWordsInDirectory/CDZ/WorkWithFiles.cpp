#include "WorkWithFiles.h"
#include <iostream>
#include <io.h>
#include <vector>
using namespace std;

WorkWithFiles::WorkWithFiles()
{
}

WorkWithFiles::~WorkWithFiles()
{
}

vector<string> WorkWithFiles::GetFilesFromDirectory(string directoryPath)
{
	vector<std::string> filesPaths;

	string searchTemplate = directoryPath + "*.txt";

	_finddata_t find;

	intptr_t result = _findfirst(searchTemplate.c_str(), &find);

	int flag = result;
	while (flag != -1)
	{
		string filePath = directoryPath + find.name;
		filesPaths.push_back(filePath);

		flag = _findnext(result, &find);
	}

	return filesPaths;
}

int WorkWithFiles::CountWordsInDirectory(string pathToDirectory, string word)
{
	vector<string> filesPaths = WorkWithFiles::GetFilesFromDirectory(pathToDirectory);
	int count = 0;

	for (int i = 0; i < filesPaths.size(); i++)
		count += CountWordsInFile(filesPaths.at(i), word);

	return count;
}

string WorkWithFiles::GetFileContent(string filePath)
{
	FILE *f = fopen(filePath.c_str(), "r");
	int size = _filelength(_fileno(f));
	char *fileContent = new char[size + 1];
	fgets(fileContent, size + 1, f);
	fclose(f);

	return fileContent;
}

int WorkWithFiles::CountWordsInFile(string pathToFile, string word)
{
	string fileContentString = GetFileContent(pathToFile);

	int count = CountWordsInString(fileContentString, word);

	return count;
}

int WorkWithFiles::CountWordsInString(string fileContentString, string word)
{
	int count = 0;
	int pos = 0;

	while (pos != -1)
	{
		pos = fileContentString.find(word, pos);

		if (pos != -1)
		{
			count++;
			pos++;
		}
	}

	return count;
}