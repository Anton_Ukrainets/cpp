#include "calc.h"
#include <iostream>
#include <string>
using namespace std;

calc::calc()
{
}


calc::~calc()
{
}

int calc::Calculate(const std::string &inputData)
{
	std::vector<int> digit = calc::Parse(inputData);

	char operation = calc::GetOperation(inputData);

	int a = digit.at(0);
	int b = digit.at(1);

	int result = calc::Calculate(a, b, operation);
	
	return result;
}

int calc::Calculate(int a, int b, char operation)
{
	int result = 0;

	if (operation == '+')
		result = a + b;

	else if (operation == '-')
		result = a - b;

	else if (operation == '*')
		result = a * b;

	else if (operation == '/')
		result = a / b;

	else if (operation == '%')
		result = a % b;

	return result;
}

bool calc::IsOperator(char inputData)
{
	if (inputData == '+' 
		|| inputData == '-' 
		|| inputData == '*' 
		|| inputData == '/' 
		|| inputData == '%')
		return true;
	return false;
}

std::vector<int> calc::Parse(const std::string &inputData)
{
	std::vector<int> digits;

	string digit = "";

	for (size_t i = 0; i <= inputData.length(); i++)
	{
		char symbol = inputData[i];

		if (IsOperator(symbol) || symbol == '\0')
		{
			digits.push_back(atoi(digit.c_str()));

			digit = "";
		}
		else
		{
			digit = digit + symbol;
		}
	}

	return digits;
}

char calc::GetOperation(const std::string & inputData)
{
	char symbol;

	for (int i = 0;i < inputData.length();i++)
	{
		if (inputData[i] == '+'
			|| inputData[i] == '-'
			|| inputData[i] == '*'
			|| inputData[i] == '/'
			|| inputData[i] == '%')
		{
			symbol = inputData.at(i);
		}
	}

	return symbol;
}