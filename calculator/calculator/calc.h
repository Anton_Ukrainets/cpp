#include <iostream>
#include <vector>
#pragma once
class calc
{
public:
	calc();
	~calc();

	int Calculate(const std::string &inputData);
private:
	bool IsOperator(char inputData);
	std::vector<int> Parse(const std::string &inputData);
	char GetOperation(const std::string &inputData);
	int Calculate(int a, int b, char operation);
};