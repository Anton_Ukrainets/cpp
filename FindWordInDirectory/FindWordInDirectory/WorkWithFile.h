#include <vector>
#pragma once
class WorkWithFile
{
public:
	WorkWithFile();
	~WorkWithFile();

	std::string FoundTextFileInDirectory(const std::string &pathToDirectory, const std::string &word);
private:
	std::vector<std::string> GetFilesFromDirectory(const std::string &pathToFile);
	bool ContainsWord(const std::string &fileContentString, const std::string &word);
	std::string GetFileContent(const std::string &filePath);
	bool ContainsWordInFile(const std::string &pathToFile, const std::string &word);
};