#include "WorkWithFile.h"
#include <io.h>

WorkWithFile::WorkWithFile()
{
}


WorkWithFile::~WorkWithFile()
{
}

std::vector<std::string> WorkWithFile::GetFilesFromDirectory(const std::string &directoryPath)
{
	std::vector<std::string> filesPaths;

	std::string searchTemplate = directoryPath + "*.txt";

	_finddata_t find;

	intptr_t result = _findfirst(searchTemplate.c_str(), &find);

	int flag = result;
	while (flag != -1)
	{
		std::string filePath = directoryPath + find.name;
		filesPaths.push_back(filePath);

		flag = _findnext(result, &find);
	}

	return filesPaths;
}

std::string WorkWithFile::FoundTextFileInDirectory(const std::string &pathToDirectory, const std::string &word)
{
	std::vector<std::string> filesPaths = WorkWithFile::GetFilesFromDirectory(pathToDirectory);

	for (int i = 0; i < filesPaths.size(); i++)
	{
		std::string filePath = filesPaths.at(i);
		bool checkPath = ContainsWordInFile(filePath, word);
		if (checkPath)
			return filePath;
	}

	return NULL;
}

std::string WorkWithFile::GetFileContent(const std::string &filePath)
{
	FILE *f = fopen(filePath.c_str(), "r");
	int size = _filelength(_fileno(f));
	char *fileContent = new char[size + 1];
	fgets(fileContent, size + 1, f);
	fclose(f);

	return fileContent;
}

bool WorkWithFile::ContainsWordInFile(const std::string &pathToFile, const std::string &word)
{
	std::string fileContentString = GetFileContent(pathToFile);

	bool containsWord = ContainsWord(fileContentString, word);

	if (containsWord)
		return true;

	return false;
}

bool WorkWithFile::ContainsWord(const std::string &fileContentString, const std::string &word)
{
	int pos = 0;
	pos = fileContentString.find(word, pos);

	if (pos != -1)
		return true;
	return false;
}