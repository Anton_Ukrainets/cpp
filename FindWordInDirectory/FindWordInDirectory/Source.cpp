#include <iostream>
#include "WorkWithFile.h"

int main()
{
	setlocale(LC_ALL, "Russian");

	const int size = 256;
	char pathToDirectory[size];
	char word[size];

	std::cout << "������� ���� � ��������:";
	std::cin >> pathToDirectory;

	std::cout << "������� ������� �����:";
	std::cin >> word;

	WorkWithFile w;
	
	std::string filePath = w.FoundTextFileInDirectory(pathToDirectory, word);

	std::cout << filePath.c_str() << std::endl;
}
/*�������7:
�������� ���������, ������� ������ ������ ���� ������ ��������� ������ � �����, ������� ����������. 
������������ ����� � ��������� �������� � �� ���� ��������� ��������� ��������� ������. 
��������� ���� ������ �������, ������� ����������, ����������� �����. 
���� ������� ����� �������, ������� �� ������� ������ ���� � ���������� �����, � ������� ������ ����� � ���������� �����.*/