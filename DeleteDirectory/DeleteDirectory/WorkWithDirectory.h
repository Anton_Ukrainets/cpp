#include <vector>
#pragma once
class WorkWithDirectory
{
public:
	WorkWithDirectory();
	~WorkWithDirectory();

	void DeleteDirectory(const std::string &directoryPath);
};