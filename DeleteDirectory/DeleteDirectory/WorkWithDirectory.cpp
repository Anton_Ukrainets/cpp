#include "WorkWithDirectory.h"
#include <io.h>
#include <direct.h>

WorkWithDirectory::WorkWithDirectory()
{
}


WorkWithDirectory::~WorkWithDirectory()
{
}

void WorkWithDirectory::DeleteDirectory(const std::string &directoryPath)
{
	_finddata_t find;

	std::string searchTemplate = directoryPath + "\\*";

	intptr_t result = _findfirst(searchTemplate.c_str(), &find);

	int flag = result;

	while (flag != -1)
	{
		std::string itemName = find.name;

		if (itemName != "."
			&& itemName != "..")
		{
			std::string itemPath = directoryPath + "\\" + itemName;

			if (find.attrib != _A_SUBDIR)
			{
				remove(itemPath.c_str());
			}
			else if (find.attrib == _A_SUBDIR)
			{
				WorkWithDirectory::DeleteDirectory(itemPath);
			}
		}

		flag = _findnext(result, &find);
	}

	_findclose(result);

	_rmdir(directoryPath.c_str());
}