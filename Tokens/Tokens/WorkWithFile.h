#include <vector>
#pragma once
class WorkWithFile
{
public:
	WorkWithFile();
	~WorkWithFile();

	std::vector<std::string> Split(const std::string &string, const std::string &separator);
private:
	std::string GetFileContent(const std::string &filePath);
};