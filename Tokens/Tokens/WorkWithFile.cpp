#include "WorkWithFile.h"
#include <io.h>

WorkWithFile::WorkWithFile()
{
}

WorkWithFile::~WorkWithFile()
{
}

std::vector<std::string> WorkWithFile::Split(const std::string &pathToFile, const std::string &separator)
{
	std::string contains = WorkWithFile::GetFileContent(pathToFile);
	std::vector<std::string> splitedString;

	char *token = strtok(strdup(contains.c_str()), separator.c_str());

	while (token != NULL)
	{
		splitedString.push_back( token);
		token = strtok(NULL, separator.c_str());
	}

	return splitedString;
}

std::string WorkWithFile::GetFileContent(const std::string &pathToFile)
{
	FILE *f = fopen(pathToFile.c_str(), "r");
	int size = _filelength(_fileno(f));
	char *fileContent = new char[size + 1];
	fgets(fileContent, size + 1, f);
	fclose(f);

	return fileContent;
}