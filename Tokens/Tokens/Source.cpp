#include <iostream>
#include "WorkWithFile.h"

int main()
{
	setlocale(LC_ALL, "Russian");

	const int size = 256;

	char pathToFile[size];
	char separator[size];

	std::cout << "������� ���� � �����:";
	std::cin >> pathToFile;

	std::cout << "������� ������ �����������:";
	std::cin >> separator;

	WorkWithFile w;
	std::vector<std::string> splitString = w.Split(pathToFile, separator);

	for (size_t i = 0; i < splitString.size(); i++)
		std::cout << splitString.at(i).c_str() << std::endl;
}
/*�������2:
�������� ���������, ������� ������ ������ ������ ���� � ���������� ����� � ������ �����������. 
��������� ���������� ����� � ������, ����� ��� �� ������� � ������� �� ������� ���������.*/