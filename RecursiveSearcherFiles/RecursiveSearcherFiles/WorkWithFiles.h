#include <vector>
#pragma once
class WorkWithFiles
{
public:
	WorkWithFiles();
	~WorkWithFiles();

	void SavePathsInFile(const std::string &pathToDirectory, const std::string &maskFile);
private:
	std::vector<std::string> GetFilesPaths(const std::string &pathToDirectory, const std::string &maskFile);
};