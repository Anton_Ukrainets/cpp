#include <iostream>
#include "WorkWithFiles.h"

int main()
{
	setlocale(LC_ALL, "Russian");

	const int size = 1024;

	char pathToDirectory[size];
	char maskFile[size];

	std::cout << "������� ���� � �����:";
	std::cin >> pathToDirectory;

	std::cout << "������� ����� �����:";
	std::cin >> maskFile;

	WorkWithFiles w;
	w.SavePathsInFile(pathToDirectory, maskFile);
}
/*�������4:
�������� ���������, ������� ������ ������ ���� � ����� ������. 
��������� � ������������ ����������� ����� ���� ��������� ��������� � ������, 
�������� ������ ���� ��������� ��������� � ������ � ��������� ���� Report.txt. 
����� ��������� ������, ���� ���� ���-�� ���� ��������� � Report.txt, ������� � ��������.*/