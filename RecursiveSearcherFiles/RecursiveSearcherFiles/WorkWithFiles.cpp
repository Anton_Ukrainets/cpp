#include "WorkWithFiles.h"
#include <io.h>

WorkWithFiles::WorkWithFiles()
{
}

WorkWithFiles::~WorkWithFiles()
{
}

void WorkWithFiles::SavePathsInFile(const std::string &pathToDirectory, const std::string &maskFile)
{
	std::string reportFilePath = pathToDirectory + "Report.txt";

	FILE *f = fopen(reportFilePath.c_str(), "w");

	std::vector<std::string> files = WorkWithFiles::GetFilesPaths(pathToDirectory, maskFile);
	
	for (size_t i = 0; i < files.size(); i++)
		fputs((files.at(i) + "\n").c_str(), f);

	fclose(f);

	system(reportFilePath.c_str());
}

std::vector<std::string> files;

std::vector<std::string> WorkWithFiles::GetFilesPaths(const std::string &pathToDirectory, const std::string &maskFile)
{
	_finddata_t find;

	std::string searchTemplate = pathToDirectory + "\\" + maskFile;

	intptr_t result = _findfirst(searchTemplate.c_str(), &find);

	int flag = result;

	while (flag != -1)
	{
		std::string itemName = find.name;

		if (itemName != "."
			&& itemName != "..")
		{
			std::string itemPath = pathToDirectory + "\\" + itemName;

			if (find.attrib != _A_SUBDIR)
			{
				files.push_back(itemPath.c_str());
			}
			else if (find.attrib == _A_SUBDIR)
			{				
				files.push_back(itemPath.c_str());
				WorkWithFiles::GetFilesPaths(itemPath, maskFile);
			}
		}

		flag = _findnext(result, &find);
	}

	_findclose(result);

	return files;
}